﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV3_Z2
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private Random randomGenerator;
        private MatrixGenerator()
        {
            randomGenerator = new Random();
        }
        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }
        public double[][] NewMatrix(int width, int height)
        {
            double[][] matrix = new double[width][];
            for (int i = 0; i < width; i++)
            {
                matrix[i] = new double[width];
            }
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    matrix[i][j] = randomGenerator.NextDouble();
                }
            }
            return matrix;
        }

    }
}
