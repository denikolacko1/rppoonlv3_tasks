﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV3_Z2
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrixGenerator matrixGenerator = MatrixGenerator.GetInstance();
            double[][] matrix = matrixGenerator.NewMatrix(3, 3);
            foreach (double[] row in matrix)
            {
                foreach (double element in row)
                {
                    Console.Write(element + " ");
                }
                Console.WriteLine();
            }


        }
    }
}
