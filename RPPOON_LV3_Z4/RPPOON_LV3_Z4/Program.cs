﻿using System;

namespace RPPOON_LV3_Z4
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification notification = new ConsoleNotification("Hello", "World", "World Notification", DateTime.Now, 
            Category.ALERT, ConsoleColor.DarkCyan);
            NotificationManager asistance = new NotificationManager();
            asistance.Display(notification);
        }
    }
}
