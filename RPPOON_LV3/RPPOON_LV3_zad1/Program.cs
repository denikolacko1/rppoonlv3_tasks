﻿using System;
using System.Threading.Tasks;
using System.Text;
using System.Collections.Generic;

namespace RPPOON_LV3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset(@"D:\RPPOON_LV3\csv.txt");
            foreach (List<string> list in data.GetData())
            {
                foreach (string msg in list)
                {
                    System.Diagnostics.Debug.WriteLine(msg);
                }
            }
            Dataset newData = (Dataset)data.Clone();

            foreach (List<string> list in newData.GetData())
            {
                foreach (string msg in list)
                {
                    System.Diagnostics.Debug.WriteLine(msg);
                }
            }

        }
    }
}
