﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV3_Z3
{
    class Logger
    {
        private static Logger instance;
        private string filePath;
        public Logger()
        {
            this.filePath = "D:\\RPPOON_LV3_Z3\\log.txt";
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void setFilePath(string filePath)
        {
            this.filePath = filePath;
        }
        public void Load(string message)
        {
            using (System.IO.StreamWriter reader = new System.IO.StreamWriter(filePath))
            {
                reader.WriteLine(message);
            }
        }
        
    }
}
